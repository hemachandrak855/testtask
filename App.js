import React, { useState, useEffect } from 'react';
import { Provider } from 'react-redux';
import reduxStore from './src/redux/store';
import { NavigationContainer } from '@react-navigation/native';
import { HomeStackNavigator, AuthStackNavigator } from './src/navigation';
import auth from '@react-native-firebase/auth';
import * as AsyncStorage from './src/asyncStore';

const App = () => {

  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  function onAuthStateChanged(user) {
    console.log('user: ', user);
    setUser(user);
    if (user) {
      AsyncStorage.storeData(AsyncStorage.Keys.USER_ID, user.uid ? user.uid : '')
    } else {
      AsyncStorage.storeData(AsyncStorage.Keys.USER_ID, '')
    }
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  return (
    <Provider store={reduxStore}>
      <NavigationContainer>
        {user ? <HomeStackNavigator /> : <AuthStackNavigator />}
      </NavigationContainer>
    </Provider>
  )

};

export default App;
