import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SignInScreen from '../screens/signIn_screen';

const Stack = createStackNavigator();

const AuthStackNavigator = () => {
    return (
        <Stack.Navigator initialRouteName={'Signin'}>
            <Stack.Screen name="Signin" component={SignInScreen} />
        </Stack.Navigator>
    );
}

export { AuthStackNavigator };