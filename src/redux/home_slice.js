import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"


function filterItems(arr, query) {
    return arr.filter(function (item) {
        return item.title.toLowerCase().indexOf(query.toLowerCase()) !== -1
    })
}

const slice = createSlice({
    name: "HOME",
    initialState: {
        search: "",
        isLoading: false,
        todosList: [],
        isSearchSarted: false,
        filterDataList: []
    },
    reducers: {
        setTodos: (state, action) => {
            const jsonAry = JSON.parse(action.payload);
            state.todosList = jsonAry;
        },
        searchText: (state, action) => {
            state.search = action.payload;
            state.isSearchSarted = true;
            state.filterDataList = filterItems(state.todosList, action.payload);
        },
        searchEnd: (state, action) => {
            state.isSearchSarted = false;
        }
    },
    extraReducers: (builder) => {

    }
})

export const { setTodos, searchText, searchEnd } = slice.actions;
export default slice.reducer;