import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"
import auth from '@react-native-firebase/auth';

export const createUser = createAsyncThunk('SIGN_IN/createUser', async (dataObj) => {

    console.log('email, password: ', dataObj.email, dataObj.password);
    const response = await auth().createUserWithEmailAndPassword(dataObj.email, dataObj.password)
    console.log('response: ', response);
    return response
})

const slice = createSlice({
    name: "SIGN_IN",
    initialState: {
        email: '',
        password: ''
    },
    reducers: {
        setEmail: (state, action) => {
            state.email = action.payload;
        },
        setPassword: (state, action) => {
            state.password = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(createUser.pending, (state, action) => {
            console.log('pending: ', action.payload);
        })
        builder.addCase(createUser.fulfilled, (state, action) => {
            console.log('fulfilled: ', action.payload);
        })
        builder.addCase(createUser.rejected, (state, action) => {
            console.log('rejected: ', action.payload);
        })
    }
})

export const { setEmail, setPassword } = slice.actions;
export default slice.reducer;