import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"

const slice = createSlice({
    name: "ADD_ITEM",
    initialState: {
        todo: "",
    },
    reducers: {
        setTodo: (state, action) => {
            state.todo = action.payload;
        }
    },
    extraReducers: (builder) => {

    }
})

export const { setTodo } = slice.actions;
export default slice.reducer;