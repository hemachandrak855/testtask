import { configureStore } from '@reduxjs/toolkit';
import signInReducer from './signin_slice';
import homeReducer from './home_slice';
import addItemReducer from './add_item_slice';

const store = configureStore({
    reducer: {
        signIn: signInReducer,
        home: homeReducer,
        addItem: addItemReducer
    }
});

export default store;