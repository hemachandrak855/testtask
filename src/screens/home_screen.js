import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, TextInput, TouchableOpacity, Button, ActivityIndicator, FlatList, TouchableOpacityBase } from 'react-native';
import auth from '@react-native-firebase/auth';
import { useDispatch, useSelector } from 'react-redux';
import { searchText, setTodos, searchEnd } from '../redux/home_slice';
import firestore from '@react-native-firebase/firestore';

const HomeScreen = ({ navigation }) => {

    const selector = useSelector(state => state.home);
    const dispatch = useDispatch();
    const ref = firestore().collection('todos');

    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <View style={{ paddingHorizontal: 5 }}>
                    <Button
                        onPress={signOut}
                        title="Sign Out"
                    />
                </View>
            ),
        });
    }, [navigation]);


    useEffect(() => {

        return ref.onSnapshot(querySnapshot => {
            let list = [];
            querySnapshot.forEach(doc => {
                const { title, isChecked } = doc.data();
                const obj = {
                    id: doc.id,
                    title,
                    isChecked,
                }
                list.push(obj)
            });
            let jsonString = JSON.stringify(list);
            dispatch(setTodos(jsonString));
        });
    }, [])

    const signOut = async () => {
        await auth()
            .signOut()
            .then(() => console.log('User signed out!'));
    }

    const toggleComplete = async (id, isChecked) => {
        await firestore()
            .collection('todos')
            .doc(id)
            .update({
                isChecked: !isChecked,
            });
    }

    return (
        <View style={styles.container}>
            <TextInput
                style={styles.input1}
                onChangeText={(text) => dispatch(searchText(text))}
                value={selector.search}
                placeholder="Search"
                keyboardType='default'
                onEndEditing={() => dispatch(searchEnd())}
                inlineImageLeft='loupe'
                inlineImagePadding={20}
            />
            <FlatList
                data={selector.isSearchSarted ? selector.filterDataList : selector.todosList}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => {
                    return (
                        <TouchableOpacity onPress={() => toggleComplete(item.id, item.isChecked)}>
                            <View style={styles.baseView}>
                                <Text style={{ flex: 1, fontSize: 16, fontWeight: '500' }}>{item.title}</Text>
                                <View style={[styles.subView, { backgroundColor: item.isChecked ? 'green' : 'red' }]}>
                                    <Text style={styles.text1}>{item.isChecked ? "Checked" : "Unchecked"}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />
            <View style={styles.bottomView}>
                <Button
                    title={'Add New Todo'}
                    onPress={() => navigation.navigate('AddItem')}
                />
            </View>
        </View>
    )
}

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingTop: 10,
        paddingHorizontal: 10
    },
    input1: {
        height: 50,
        fontSize: 15,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'gray',
        marginBottom: 10,
        backgroundColor: 'white'
    },
    baseView: {
        flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%', height: '100%', paddingVertical: 10, paddingHorizontal: 15, backgroundColor: 'white', borderRadius: 5, marginBottom: 5
    },
    subView: {
        flex: 0.3, padding: 10, alignItems: 'center', justifyContent: 'center'
    },
    text1: {
        fontSize: 12, fontWeight: '400', color: 'white'
    },
    bottomView: {
        marginVertical: 15
    }
})

// const user1 = auth().currentUser;
//         user1.getIdToken().then((response) => {
//             // User deleted.
//             console.log('token: ', response)

//         }).catch((error) => {
//             // An error ocurred
//             // ...
//         });