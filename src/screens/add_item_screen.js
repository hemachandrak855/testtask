import React, { useEffect, useState } from 'react';
import { SafeAreaView, StyleSheet, View, Text, TextInput, Button, Keyboard } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import { useDispatch, useSelector } from 'react-redux';
import { setTodo } from '../redux/add_item_slice';

const AddItemScreen = ({ navigation }) => {

    const selector = useSelector(state => state.addItem);
    const dispatch = useDispatch();

    useEffect(() => {

    }, [])

    const addTodo = () => {

        Keyboard.dismiss();
        if (selector.todo.length == 0) {
            return;
        }

        firestore()
            .collection('todos')
            .add({
                title: selector.todo,
                isChecked: false,
            })
            .then(() => {
                console.log('User added!');
                dispatch(setTodo(""))
                navigation.goBack();
            })
    }

    return (
        <SafeAreaView style={styles.container}>

            <TextInput
                style={styles.input}
                onChangeText={(text) => dispatch(setTodo(text))}
                value={selector.todo}
                placeholder="Add new todo"
                multiline={true}
                keyboardType={'default'}
            />
            <View style={{ height: 35 }}></View>
            <Button
                title={'Add Todo'}
                onPress={addTodo}
            />

        </SafeAreaView>
    )
}

export default AddItemScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 20
    },
    input: {
        height: 60,
        borderColor: 'gray',
        borderBottomWidth: 2,
        fontSize: 16
    },
})