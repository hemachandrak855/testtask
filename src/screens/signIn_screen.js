import React, { useEffect } from 'react';
import { View, StyleSheet, Text, TextInput, TouchableOpacity, Alert, Keyboard } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { setEmail, setPassword, createUser } from '../redux/signin_slice';
import auth from '@react-native-firebase/auth';


const SignInScreen = ({ navigation }) => {

    const dispatch = useDispatch();
    const { email, password } = useSelector(state => state.signIn);

    useEffect(() => {
        dispatch(setEmail(''));
    }, [])

    const validate = (isFromSignIn) => {

        Keyboard.dismiss();
        if (email.length === 0 && password.length === 0) {
            showAlertMessage("please enter email & password");
            return;
        }

        if (email.length === 0) {
            showAlertMessage("please enter email");
            return;
        }
        if (isEmailValid(email) === false) {
            showAlertMessage("please enter valid email-id");
            return;
        }

        if (password.length === 0) {
            showAlertMessage("please enter password");
            return;
        }

        if (password.length < 6) {
            showAlertMessage("password must be 6 characters or digits");
            return;
        }

        if (isFromSignIn) {
            signinUser(email, password);
        } else {
            createUser(email, password);
        }
    }

    const signinUser = (emailId, pass) => {

        auth()
            .signInWithEmailAndPassword(emailId, pass)
            .then(() => {
                console.log('User account created & signed in!');
            })
            .catch(error => {
                showAlertMessage(error.code);
                console.error(error);
            });
    }

    const createUser = async (emailId, pass) => {

        await auth()
            .createUserWithEmailAndPassword(emailId, pass)
            .then(() => {
                console.log('User account created & signed in!');
            })
            .catch(error => {
                if (error.code === 'auth/email-already-in-use') {
                    console.log('That email address is already in use!');
                    showAlertMessage("That email address is already in use!");
                }

                if (error.code === 'auth/invalid-email') {
                    console.log('That email address is invalid!');
                    showAlertMessage("That email address is invalid!");
                }

                console.warn(error);
            });
    }

    const isEmailValid = (email) => {
        let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        return pattern.test(String(email).toLowerCase())
    }

    const showAlertMessage = (message) => {
        Alert.alert("", message);
    }

    return (
        <View style={styles.container}>
            <TextInput
                style={styles.input}
                onChangeText={(text) => dispatch(setEmail(text))}
                value={email}
                placeholder="email"
                keyboardType='email-address'
            />
            <TextInput
                style={[styles.input, { marginTop: 20 }]}
                onChangeText={(text) => dispatch(setPassword(text))}
                value={password}
                placeholder='password'
                keyboardType={'default'}
                secureTextEntry={true}
            />
            <TouchableOpacity style={styles.button} onPress={() => validate(true)}>
                <Text style={styles.text}>Sign In</Text>
            </TouchableOpacity>
        </View>
    )
}

export default SignInScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 20
    },
    input: {
        height: 50,
        borderColor: 'gray',
        borderBottomWidth: 2,
        fontSize: 16
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'blue',
        height: 50,
        borderRadius: 25,
        marginTop: 50
    },
    text: {
        color: 'white',
        fontSize: 20
    }
})