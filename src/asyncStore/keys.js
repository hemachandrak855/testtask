

export const USER_NAME = "USER_NAME";
export const USER_ID = "USER_ID";
export const USER_TOKEN = "USER_TOKEN";
export const IS_USER_LOGGED_IN = "IS_USER_LOGGED_IN";
